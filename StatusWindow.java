// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// StatusWindow class for relaying progress and output to the user.

package AceUtil;

import java.awt.*;
import javax.swing.*;

public class StatusWindow extends JFrame {
    
    public StatusWindow(){
        buildWindow();
        
    }
    
    private void buildWindow(){
        this.setTitle("AceUtil Status Dialogue");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        
        //Row 1, Textbox
        Container textContainer = new Container();
        textContainer.setLayout(new FlowLayout());
        JTextArea jTextAreaOut = new JTextArea();
        //jTextAreaOut.setBorder(BorderFactory.createLineBorder(Color.black));
        JScrollPane jScrollPaneOut = new JScrollPane(jTextAreaOut);
        jScrollPaneOut.setPreferredSize(new Dimension(600, 350));
        jTextAreaOut.setEditable(false);
        textContainer.add(jScrollPaneOut);
        this.add(textContainer);
        
        //Row 2, Buttons
        Container buttContainer = new Container();
        buttContainer.setLayout(new FlowLayout());
        JButton jButtonCopyOutput = new JButton("Copy Output");
        JButton jButtonNewContig = new JButton("Analyze Another Contig");
        JButton jButtonNewFile = new JButton("Analyze a New File");
        JButton jButtonDismiss = new JButton("Dismiss");
        buttContainer.add(jButtonCopyOutput);
        buttContainer.add(jButtonNewContig);
        buttContainer.add(jButtonNewFile);
        buttContainer.add(jButtonDismiss);
        this.add(buttContainer);
        
        this.pack();
    }
    
    public static void main(String[] args){
        JFrame f = new StatusWindow();
        f.setVisible(true);
    }
}
