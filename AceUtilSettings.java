// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// AceUtilSettings class for tracking settings independent of windows.

package AceUtil;

import java.util.prefs.Preferences;

public class AceUtilSettings {
    
    //Variables Declaration
    private int discCutoff, discStrandCutoff, lowCutoff, lowStrandCutoff, version;
    private boolean discFind, discStrand, lowCov, lowStrand, writeOut;
    private String defaultPath;
    private Preferences userPrefs;
    
    //Default Constructor
    public AceUtilSettings(){
        
        this.userPrefs = Preferences.userNodeForPackage(AceUtilSettings.class);
        
        this.discCutoff = userPrefs.getInt("discCutoff", 18);
        this.discStrandCutoff = userPrefs.getInt("discStrandCutoff", 70);
        this.lowCutoff = userPrefs.getInt("lowCutoff", 20);
        this.lowStrandCutoff = userPrefs.getInt("lowStrandCutoff", 4);
        this.discFind = userPrefs.getBoolean("discFind", true);
        this.discStrand = userPrefs.getBoolean("discStrand", true);
        this.lowCov = userPrefs.getBoolean("lowCov", true);
        this.lowStrand = userPrefs.getBoolean("lowStrand", true);
        this.writeOut = userPrefs.getBoolean("writeOut", true);
        this.version = userPrefs.getInt("version", 3);
        this.defaultPath = userPrefs.get("defaultPath", "~");
    }
    
    public void refreshSettings(){
        this.discCutoff = userPrefs.getInt("discCutoff", 18);
        this.discStrandCutoff = userPrefs.getInt("discStrandCutoff", 70);
        this.lowCutoff = userPrefs.getInt("lowCutoff", 20);
        this.lowStrandCutoff = userPrefs.getInt("lowStrandCutoff", 4);
        this.discFind = userPrefs.getBoolean("discFind", true);
        this.discStrand = userPrefs.getBoolean("discStrand", true);
        this.lowCov = userPrefs.getBoolean("lowCov", true);
        this.lowStrand = userPrefs.getBoolean("lowStrand", true);
        this.writeOut = userPrefs.getBoolean("writeOut", true);
        this.version = userPrefs.getInt("version", 3);
        this.defaultPath = userPrefs.get("defaultPath", "~");
    }
    
    public void saveSettings(){
        this.userPrefs.putInt("discCutoff", this.discCutoff);
        this.userPrefs.putInt("discStrandCutoff", this.discStrandCutoff);
        this.userPrefs.putInt("lowCutoff", this.lowCutoff);
        this.userPrefs.putInt("lowStrandCutoff", this.lowStrandCutoff);
        this.userPrefs.putBoolean("discFind", this.discFind);
        this.userPrefs.putBoolean("discStrand",this.discStrand);
        this.userPrefs.putBoolean("lowCov", this.lowCov);
        this.userPrefs.putBoolean("lowStrand", this.lowStrand);
        this.userPrefs.putBoolean("writeOut", this.writeOut);
        this.userPrefs.putInt("version", this.version);
        this.userPrefs.put("defaultPath", this.defaultPath);
    }
    
    //Set statements
    public void setDiscCutoff(int paramInt){
        this.discCutoff = paramInt;
    }
    public void setDiscStrandCutoff(int paramInt){
        this.discStrandCutoff = paramInt;
    }
    public void setLowCutoff(int paramInt){
        this.lowCutoff = paramInt;
    }
    public void setLowStrandCutoff(int paramInt){
        this.lowStrandCutoff = paramInt;
    }
    public void setDiscFind(boolean paramBool){
        this.discFind = paramBool;
    }
    public void setDiscStrand(boolean paramBool){
        this.discStrand = paramBool;
    }
    public void setLowCov(boolean paramBool){
        this.lowCov = paramBool;
    }
    public void setLowStrand(boolean paramBool){
        this.lowStrand = paramBool;
    }
    public void setWriteOut(boolean paramBool){
        this.writeOut = paramBool;
    }
    public void setVersion(int paramInt){
        this.version = paramInt;
    }
    public void setDefaultPath(String paramString){
        this.defaultPath = paramString;
    }
    
    //Get Statements
    public int getDiscCutoff(){
        return this.discCutoff;
    }
    public int getDiscStrandCutoff(){
        return this.discStrandCutoff;
    }
    public int getLowCutoff(){
        return this.lowCutoff;
    }
    public int getLowStrandCutoff(){
        return this.lowStrandCutoff;
    }
    public boolean getDiscFind(){
        return this.discFind;
    }
    public boolean getDiscStrand(){
        return this.discStrand;
    }
    public boolean getLowCov(){
        return this.lowCov;
    }
    public boolean getLowStrand(){
        return this.lowStrand;
    }
    public boolean getWriteOut(){
        return this.writeOut;
    }
    public int getVersion(){
        return this.version;
    }
    public String getPath(){
        return this.defaultPath;
    }
}
