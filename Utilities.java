// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// Utilites class

package AceUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Scanner;

public class Utilities {
    
    //This method copies a file - it should be noted that it also overwrites the file if exists.
    public static void copyFile( File paramSource, File paramDest ) throws IOException {

        try{
            Files.copy( paramSource.toPath(), paramDest.toPath());
        }catch (FileAlreadyExistsException e){
            Files.delete(paramDest.toPath());
            Files.copy( paramSource.toPath(), paramDest.toPath());
        }
    } 
    
    public static void writeComment(BufferedWriter paramWrite, int paramStart, String paramComment, String paramContig) throws IOException{
        paramWrite.newLine();
        paramWrite.write("CT{");
        paramWrite.newLine();
        paramWrite.write(paramContig + " comment discFind " + (paramStart + 1) + " "+ (paramStart + 1) + " 130912:160000");
        paramWrite.newLine();
        paramWrite.write("COMMENT{");
        paramWrite.newLine();
        paramWrite.write(paramComment);
        paramWrite.newLine();
        paramWrite.write("C}");
        paramWrite.newLine();
        paramWrite.write("}");
        paramWrite.newLine();
    }
    
    public static void writeComment(BufferedWriter paramWrite, int paramStart, int paramStop, String paramComment, String paramContig) throws IOException{
        paramWrite.newLine();
        paramWrite.write("CT{");
        paramWrite.newLine();
        paramWrite.write(paramContig + " comment discFind " + (paramStart + 1) + " "+ (paramStart + 1) + " 130912:160000");
        paramWrite.newLine();
        paramWrite.write("COMMENT{");
        paramWrite.newLine();
        paramWrite.write(paramComment);
        paramWrite.newLine();
        paramWrite.write("C}");
        paramWrite.newLine();
        paramWrite.write("}");
        paramWrite.newLine();
    }
    
    //finds a phrase after a scanner
    public static Scanner findPhrase(Scanner paramScan, String paramPhrase) throws FileNotFoundException{
            
        Scanner readFind = paramScan;
        
        while (readFind.hasNextLine()){
        
            String find = readFind.findInLine(paramPhrase);
            if (find != null) {
                return readFind;
            }
            readFind.nextLine();
        }
        return null;
    }
   
}
