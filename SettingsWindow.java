// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// SettingsWindow class for displaying the Settings window.

package AceUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SettingsWindow extends JFrame implements ActionListener {
    
    JSlider jSliderDisc;
    JLabel jLabelSliderDisc;
    JSlider jSliderDiscStrand;
    JLabel jLabelSliderDiscStrand;
    JTextField jTextFieldLowCov;
    JTextField jTextFieldLowStrand;
    JTextField jTextFieldDir;
    JButton jButtonAccept;
    JButton jButtonSave;
    JButton jButtonCancel;
    JButton jButtonDir;
    AceUtilSettings settings;
    
    public SettingsWindow(AceUtilSettings paramAceUtilSettings){
        this.settings = paramAceUtilSettings;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }
    
    
    
    private void createAndShowGUI(){
        
        //Base Window Setup
        this.setTitle("AceUtil Settings");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        
        //Row 1, Default Path
        Container dirContainer = new Container();
        dirContainer.setLayout(new FlowLayout(FlowLayout.RIGHT));
        dirContainer.add(new JLabel("Default Directory:  "));
        jTextFieldDir = new JTextField(settings.getPath());
        jTextFieldDir.setEnabled(false);
        jTextFieldDir.setPreferredSize(new Dimension(400, 25));
        jButtonDir = new JButton("Set Directory");
        dirContainer.add(jTextFieldDir);
        dirContainer.add(jButtonDir);
        this.add(dirContainer);
        this.add(new JSeparator());
        this.add(new JSeparator());
        
        Container discCovContainer = new Container();
        discCovContainer.setLayout(new BoxLayout(discCovContainer, BoxLayout.X_AXIS));
        
        Container containerDiscGrid = new Container();
        containerDiscGrid.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        //Row 2, DiscFind Settings
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.EAST;
        containerDiscGrid.add(new JLabel("DiscFind Cutoff"), c);
        Container containerDisc = new Container();
        containerDisc.setLayout(new FlowLayout(FlowLayout.RIGHT));
        jSliderDisc = new JSlider(0, 100, settings.getDiscCutoff());
        jLabelSliderDisc = new JLabel(String.valueOf(jSliderDisc.getValue()));
        Dimension d = jLabelSliderDisc.getPreferredSize();
        jLabelSliderDisc.setPreferredSize(new Dimension(d.width + 10, d.height ));
        containerDisc.add(jSliderDisc);
        containerDisc.add(jLabelSliderDisc);
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 2;
        c.anchor = GridBagConstraints.WEST;
        containerDiscGrid.add(containerDisc, c);
        
        //Row 3 DiscStrand Settings
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.EAST;
        containerDiscGrid.add(new JLabel("  DiscStrand Cutoff"), c);
        Container containerDiscStrand = new Container();
        containerDiscStrand.setLayout(new FlowLayout(FlowLayout.RIGHT));
        jSliderDiscStrand = new JSlider(0, 100, settings.getDiscStrandCutoff());
        jLabelSliderDiscStrand = new JLabel(String.valueOf(jSliderDiscStrand.getValue()));
        d = jLabelSliderDiscStrand.getPreferredSize();
        jLabelSliderDiscStrand.setPreferredSize(new Dimension(d.width + 10, d.height ));
        containerDiscStrand.add(jSliderDiscStrand);
        containerDiscStrand.add(jLabelSliderDiscStrand);
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        c.anchor = GridBagConstraints.WEST;
        containerDiscGrid.add(containerDiscStrand, c);
        
        discCovContainer.add(containerDiscGrid);
        
        Container containerLowCov = new Container();
        containerLowCov.setLayout(new GridBagLayout());
        
        //Row 2.5 LowCov Settings
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.EAST;
        containerLowCov.add(new JLabel("LowCov Cutoff  "), c);
        c.gridx = 1;
        c.gridy = 0;
        c.anchor = GridBagConstraints.WEST;
        jTextFieldLowCov = new JTextField(4);
        jTextFieldLowCov.setHorizontalAlignment(JTextField.RIGHT);
        jTextFieldLowCov.setText(String.valueOf(settings.getLowCutoff()));
        containerLowCov.add(jTextFieldLowCov, c);
        
        //Row 4 LowString Settings
        c.gridx = 0;
        c.gridy = 1;
        c.anchor = GridBagConstraints.EAST;
        containerLowCov.add(new JLabel("LowStrand Cutoff  "), c);
        c.gridx = 1;
        c.gridy = 1;
        c.anchor = GridBagConstraints.WEST;
        jTextFieldLowStrand = new JTextField(4);
        jTextFieldLowStrand.setHorizontalAlignment(JTextField.RIGHT);
        jTextFieldLowStrand.setText(String.valueOf(settings.getLowStrandCutoff()));
        containerLowCov.add(jTextFieldLowStrand, c);
        
        discCovContainer.add(new JSeparator(SwingConstants.VERTICAL));
        discCovContainer.add(new JSeparator(SwingConstants.VERTICAL));
        discCovContainer.add(Box.createHorizontalStrut(5));
        discCovContainer.add(containerLowCov);
        discCovContainer.add(Box.createHorizontalStrut(5));
        this.add(discCovContainer);
        this.add(new JSeparator());
        this.add(new JSeparator());
        
        //The buttons
        Container buttContainer = new Container();
        buttContainer.setLayout(new FlowLayout());
        jButtonAccept = new JButton("Accept");
        jButtonSave = new JButton("Accept and Save");
        jButtonCancel = new JButton("Cancel");
        buttContainer.add(jButtonAccept);
        buttContainer.add(jButtonSave);
        buttContainer.add(jButtonCancel);
        this.add(buttContainer);
        
        //Listeners
        jSliderDisc.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent evt){
                jLabelSliderDisc.setText(String.valueOf(jSliderDisc.getValue()));
            }
        });
        jSliderDiscStrand.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent evt){
                jLabelSliderDiscStrand.setText(String.valueOf(jSliderDiscStrand.getValue()));
            }
        });
        jButtonAccept.addActionListener(this);
        jButtonSave.addActionListener(this);
        jButtonCancel.addActionListener(this);
        jButtonDir.addActionListener(this);
        
        //HouseKeeping
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        Object actionSource = ae.getSource();
        if (actionSource == jButtonDir){
            
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("~"));
            chooser.setDialogTitle("Choose Default Directory");
            chooser.setMultiSelectionEnabled(false);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String defPath = (chooser.getSelectedFile()).getAbsolutePath();
                this.jTextFieldDir.setText(defPath);
                settings.setDefaultPath(defPath);
            }
            
        }else if (actionSource == jButtonAccept){
            try{
                settings.setDiscCutoff(jSliderDisc.getValue());
                settings.setDiscStrandCutoff(jSliderDiscStrand.getValue());
                settings.setLowCutoff(Integer.parseInt(jTextFieldLowCov.getText()));
                settings.setLowStrandCutoff(Integer.parseInt(jTextFieldLowStrand.getText()));
                this.dispose();
            }catch (Exception e){
                JOptionPane.showMessageDialog(null, "LowCov/Strand accepts numeric values only.", "Error!", JOptionPane.WARNING_MESSAGE);
            }
        }else if (actionSource == jButtonSave){
            try{
                settings.setDiscCutoff(jSliderDisc.getValue());
                settings.setDiscStrandCutoff(jSliderDiscStrand.getValue());
                settings.setLowCutoff(Integer.parseInt(jTextFieldLowCov.getText()));
                settings.setLowStrandCutoff(Integer.parseInt(jTextFieldLowStrand.getText()));
                settings.saveSettings();
                JOptionPane.showMessageDialog(null, "Settings Saved", "Success!", JOptionPane.INFORMATION_MESSAGE);
                this.dispose();
            }catch (Exception e){
                JOptionPane.showMessageDialog(null, "LowCov/Strand accepts numeric values only.", "Error!", JOptionPane.WARNING_MESSAGE);
            }
        }else if (actionSource == jButtonCancel){
            this.dispose();
        }
    }
}