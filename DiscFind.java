// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// Discfind class for building and analyzing 1.0

package AceUtil;

//import bloc
import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

        


public class DiscFind {
    
    static AceUtilSettings settings;
    
    public static File paramFile; 
    //The array that holds all that data - HAHA I INCLUDED THE WORD ASS
    public static char[] ass;
    //Tot holds total number of bases at position, disc holds total discrepancy numbers
    public static int[] tot, disc, totF, discF, totR, discR;
    public static float[] perDisc, perDiscF, perDiscR;
    public static ArrayList<String> readNames;
    public static int discCount, discCountF, discCountR, lowCount, lowRegions;
    
    public static HashMap seq;
    public static HashMap start;
    public static HashMap dir;
    
    
    public static void copyFile( File paramSource, File paramDest ) throws IOException {
        
        try{
            Files.copy( paramSource.toPath(), paramDest.toPath());
        }catch (FileAlreadyExistsException FAEe){
            Files.delete(paramDest.toPath());
            Files.copy( paramSource.toPath(), paramDest.toPath());
        }
    }
    
    //finds a phrase after a scanner
    public static Scanner findPhrase(Scanner paramScan, String paramPhrase) throws FileNotFoundException{
        Scanner readFind = paramScan;
        while (readFind.hasNextLine()){
            String find = readFind.findInLine(paramPhrase);
            if (find != null) {
                return readFind;
            }
            readFind.nextLine();
        }
        return null;
    }
    
    //sets up the arrays representing the consensus, total coverage, and discrepencies
    public static Scanner buildArray(String paramName) throws FileNotFoundException{
            
        //array size
        int x;
            
        //System.out.println("Initializing Scanner");
        Scanner localScanner = new Scanner(new FileReader(paramFile));
        //System.out.println("Scanner Initialized");
        
        while (localScanner.hasNextLine()){
            //System.out.println(".."+"CO "+paramName+"..");
            String find = localScanner.findInLine("CO " + paramName);
            //System.out.println(find);
            if (find != null) {
                //System.out.println("foundit");
                x = localScanner.nextInt();
                //System.out.println("Consensus Length (w/Pads) is " + x);
                
                //This block defines all of the arrays holding discrepant data
                ass = new char[x];
                tot = new int[x];
                disc = new int[x];
                totF = new int[x];
                discF = new int[x];
                totR = new int[x];
                discR = new int[x];
                perDisc = new float[x];
                perDiscF = new float[x];
                perDiscR = new float[x];
                
                int numReads = localScanner.nextInt();
                seq = new HashMap(numReads);
                start = new HashMap(numReads);
                dir = new HashMap(numReads);
                readNames = new ArrayList(numReads);
                //System.out.println("...");
                //System.out.println("Consensus grid initialized with length " + x);
                break;
            }
            localScanner.nextLine();
        }
        
        int pos = 0;
        localScanner.nextLine();
        String current = localScanner.nextLine();
        current = current.toUpperCase();
                
        //System.out.println(current);
        
        while (!"".equals(current)){
            for (int i = 0; i < current.length();i++){
                ass[i+pos] = current.charAt(i);
            }
            pos += current.length();
            current = localScanner.nextLine();
            current = current.toUpperCase();
        }
        //System.out.println(ass);
        
        //System.out.println("Consensus grid filled with sequence...");
        
        return localScanner;
            
    }
    
    //Builds HashMap of start positions
    public static Scanner buildStarts(Scanner paramScan) throws FileNotFoundException{
        paramScan = findPhrase(paramScan, "AF");
        
        String AF;
        System.out.println("BEFOREWHILE");
        do{
        String current = paramScan.next();
        //System.out.println(current);
        dir.put(current, paramScan.next());         //Grabs the U/C designation
        start.put(current, paramScan.nextInt());
        readNames.add(current);
        AF = paramScan.next();
        }while ("AF".equals(AF));
        /*if ("BS".equals(AF)){
            do{
                paramScan.next();
                paramScan.next();
                paramScan.next();
                AF =p aramScan.next();
            }while ("BS".equals(AF));
        }*/
        
        //System.out.println("Start Coordinates Logged...");
        
        return paramScan;
    }
    
    //builds HashMap of reads
    public static void buildReads(Scanner paramScan) throws FileNotFoundException{
        
        boolean firstLoop = true;
        
        do{
            if (!firstLoop){
                //System.out.println("firstLoop");
                paramScan = findPhrase(paramScan, "RD");
                if (paramScan == null){
                    break;
                }
            }
            
            String key = paramScan.next();
            //System.out.println(key);
            paramScan.nextLine();
            //System.out.println(current);
            String readParse = "";
            String current;
            if (start.containsKey(key)){
                current = paramScan.nextLine();
                //System.out.println(current);
                while (!"".equals(current)){
                    readParse += current;
                    current = paramScan.nextLine();
                    //System.out.println(current);
                }
                readParse = readParse.toUpperCase();
                seq.put(key, readParse);
                    
                
            }    
            //System.out.println();
            //System.out.println(readParse);
            firstLoop = false;
            //System.out.println();
            
        }while (paramScan != null && paramScan.hasNext());
        
        //System.out.println("Read Contents Logged...");
        
    }

    //Calculates the total descrepancies at each position
    public static void analysis(String paramContig, String paramWriteFile) throws FileNotFoundException, IOException{
        
        System.out.println("Analysis Starting");
        
        for(int j = 0; j < readNames.size();j++){

            String key = readNames.get(j);
            String read = (seq.get(key)).toString();
            int readStart = (Integer)start.get(key);

            for(int i = 0; i < read.length();i++){

                if ((readStart < 1)&&(i == 0)){
                    i = Math.abs(readStart);
                }
                if (readStart + i < 1) {
                    continue;
                }
                if (readStart + i > ass.length) {
                    break;
                }
                tot[i+readStart-1] += 1;
                if (!(ass[i+readStart-1]==read.charAt(i))){
                    disc[i+readStart-1] += 1;
                }
                
                if (dir.get(key).equals("U")){
                    totF[i + readStart - 1] += 1;
                    if (!(ass[i+readStart-1]==read.charAt(i))){
                        discF[i + readStart - 1] += 1;
                    }
                }else if (dir.get(key).equals("C")){
                    totR[i + readStart - 1] += 1;
                    if (!(ass[i+readStart-1]==read.charAt(i))){
                        discR[i + readStart - 1] += 1;
                    }
                }
                
            }
        }

        for(int i=0;i < perDisc.length;i++){
            perDisc[i] = (float)disc[i]/tot[i];
        }
        
        for(int i=0;i < perDiscF.length;i++){
            perDiscF[i] = (float)discF[i]/totF[i];
        }
        
        for(int i=0;i < perDiscR.length;i++){
            perDiscR[i] = (float)discR[i]/totR[i];
        }

        if (!settings.getWriteOut()){
            System.out.println("no writeOut");
            System.out.println("discFind = " + settings.getDiscFind());
            if (settings.getDiscFind()){
                System.out.println("discFind no writeOut");
                discCount = 0;
                for(int i = 0;i < perDisc.length;i++){
                    if ((perDisc[i] * 100) >= (settings.getDiscCutoff())){
                        discCount++;
                    }
                }
                System.out.println("discFind analysis finished - " + discCount + " discrepancies at " + settings.getDiscCutoff() + "% cutoff.");
            }
            
            if (settings.getDiscStrand()){
                
                System.out.println("discStrand");
                discCountF = 0;
                for(int i = 0;i < perDiscF.length;i++){
                    if ((perDiscF[i] * 100) >= (settings.getDiscStrandCutoff())){
                        discCountF++;
                    }
                }
                System.out.println("discFind analysis finished - " + discCountF + " FORWARD discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");
                
                discCountR = 0;
                for(int i = 0;i < perDiscR.length;i++){
                    if ((perDiscR[i] * 100) >= (settings.getDiscStrandCutoff())){
                        discCountR++;
                    }
                }
                System.out.println("discFind analysis finished - " + discCountR + " REVERSE discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");
                
            }
            
            System.out.println("lowCov = " + settings.getLowCov());
            if (settings.getLowCov()){
                System.out.println("lowCov no writeOut");
                lowCount = 0;
                lowRegions = 0;
                int regStart = 0;
                int regCov = 0;
                int regSize = 0;
                boolean prevLow = false;
                for(int i = 0;i < tot.length;i++){
                    if (prevLow){
                        if (tot[i] <= settings.getLowCutoff()){
                            regCov += tot[i];
                            regSize++;
                            lowCount++;
                            continue;
                        }
                        lowRegions++;
                        prevLow = false;

                    }
                    if (tot[i] <= settings.getLowCutoff()){
                        lowCount++;
                        prevLow = true;
                        regStart = i;
                    }
                }
                if (prevLow){
                    lowRegions++;
                }
                System.out.println("lowCov analysis finished - " + lowCount + " low coverage bases and " + lowRegions + " regions of low coverage at " + settings.getLowCutoff() + "x cutoff.");
                
                if (regSize != 0){
                    System.out.println("If you are checking the total coverage, it is " + (regCov/regSize) + "x");
                }
            }
        }


        if (settings.getWriteOut()){

            System.out.println("Making the new file");
            
            File writeOut = new File(paramWriteFile);

            copyFile(paramFile, writeOut);
            
            FileWriter getFile = new FileWriter(writeOut, true);
            BufferedWriter out = new BufferedWriter(getFile);

            if (settings.getDiscFind()){
                System.out.println("discFind writeOut");
                discCount = 0;
                for(int i = 0;i < perDisc.length;i++){
                    if ((perDisc[i] * 100) >= (settings.getDiscCutoff())){
                        discCount++;
                        out.newLine();
                        out.write("CT{");
                        out.newLine();
                        out.write(paramContig + " comment discFind " + (i + 1) + " "+ (i + 1) + " 120830:132000");
                        out.newLine();
                        out.write("COMMENT{");
                        out.newLine();
                        out.write((perDisc[i] * 100) + "% discrepancies at this position greater than cutoff " + settings.getDiscCutoff() + "%");
                        out.newLine();
                        out.write("C}");
                        out.newLine();
                        out.write("}");
                        out.newLine();
                    }
                }
                System.out.println("discFind analysis finished - " + discCount + " discrepancies at " + settings.getDiscCutoff() + "% cutoff.");
            }
            
            if (settings.getDiscStrand()){
                
                System.out.println("discStrand");
                discCountF = 0;
                for(int i = 0;i < perDiscF.length;i++){
                    if ((perDiscF[i] * 100) >= (settings.getDiscStrandCutoff())){
                        discCountF++;
                        out.newLine();
                        out.write("CT{");
                        out.newLine();
                        out.write(paramContig + " comment discStrand " + (i + 1) + " "+ (i + 1) + " 120830:132000");
                        out.newLine();
                        out.write("COMMENT{");
                        out.newLine();
                        out.write((perDiscF[i] * 100) + "% FORWARD discrepancies at this position greater than cutoff " + settings.getDiscStrandCutoff() + "%");
                        out.newLine();
                        out.write("C}");
                        out.newLine();
                        out.write("}");
                        out.newLine();
                    }
                }
                System.out.println("discFind analysis finished - " + discCountF + " FORWARD discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");
                
                discCountR = 0;
                for(int i = 0;i < perDiscR.length;i++){
                    if ((perDiscR[i] * 100) >= (settings.getDiscStrandCutoff())){
                        discCountR++;
                        out.newLine();
                        out.write("CT{");
                        out.newLine();
                        out.write(paramContig + " comment discStrand " + (i + 1) + " "+ (i + 1) + " 120830:132000");
                        out.newLine();
                        out.write("COMMENT{");
                        out.newLine();
                        out.write((perDiscR[i] * 100) + "% REVERSE discrepancies at this position greater than cutoff " + settings.getDiscStrandCutoff() + "%");
                        out.newLine();
                        out.write("C}");
                        out.newLine();
                        out.write("}");
                        out.newLine();
                    }
                }
                System.out.println("discFind analysis finished - " + discCountR + " REVERSE discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");
                
            }

            if (settings.getLowCov()){
                System.out.println("lowCov writeOut");
                lowCount = 0;
                lowRegions = 0;
                int regStart = 0;
                int regCov = 0;
                int regSize = 0;
                boolean prevLow = false;

                for(int i = 0;i < tot.length;i++){
                    if (prevLow){
                        if (tot[i] <= settings.getLowCutoff()){
                            regCov += tot[i];
                            regSize++;
                            lowCount++;
                            continue;
                        }
                        
                        lowRegions++;
                            
                        out.newLine();
                        out.write("CT{");
                        out.newLine();
                        out.write(paramContig + " comment lowCov " + (regStart + 1) + " "+ (i) + " 120717:163000");
                        out.newLine();
                        out.write("COMMENT{");
                        out.newLine();
                        out.write("Avg " + (regCov/regSize) + "x depth coverage at psoition " + (regStart + 1) + "-" + i + " lower than cutoff " + settings.getLowCutoff() + "x");
                        out.newLine();
                        out.write("C}");
                        out.newLine();
                        out.write("}");
                        out.newLine();
                        
                        prevLow = false;
                        regCov = 0;
                        regSize = 0;

                    }
                    if (tot[i] <= settings.getLowCutoff()){
                        lowCount++;
                        regCov += tot[i];
                        regSize++;
                        prevLow = true;
                        regStart = i;
                    }
                }
                
                if (prevLow){

                        lowRegions++;
                            
                        out.newLine();
                        out.write("CT{");
                        out.newLine();
                        out.write(paramContig + " comment lowCov " + (regStart + 1) + " "+ (tot.length) + " 120717:163000");
                        out.newLine();
                        out.write("COMMENT{");
                        out.newLine();
                        out.write("Avg " + (regCov/regSize) + "x depth coverage at psoition " + (regStart + 1) + "-" + (tot.length) + " lower than cutoff " + settings.getLowCutoff() + "x");
                        out.newLine();
                        out.write("C}");
                        out.newLine();
                        out.write("}");
                        out.newLine();
                        

                    }
            
                System.out.println("lowCov analysis finished - " + lowCount + " low coverage bases and " + lowRegions + " regions of low coverage at " + settings.getLowCutoff() + "x cutoff.");
            }

            out.close();
        }
    }
    
    public static void process(File paramInputFile, String paramContig, String outputFile, AceUtilSettings paramAceUtilSettings) throws FileNotFoundException, IOException{
        settings = paramAceUtilSettings;
        paramFile = paramInputFile;
        buildReads(buildStarts(buildArray(paramContig)));
        analysis(paramContig, outputFile);
        
    }
}