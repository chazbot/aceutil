// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 20, 2013
// AssemblyWorker class for large assemblies, v3.0

package AceUtil;

//Import Block
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

public class AssemblyWorker extends SwingWorker<Void, Integer> {
    
    AceUtilSettings settings;
    
    public MainWindow mw;
    
    private int assemblySize;           //length of assembly (with pads)
    private int numReads;
    private int curRead = 0;
    
    private File inputFile;
    
    private char[] assembly;    //Holds assembly consensus
    private int[] totalDepth;
    private int[] discDepth;
    private int[] unCompDepth;
    private int[] unCompDiscDepth;
    private int[] compDepth;
    private int[] compDiscDepth;
    private float[] perDisc;
    private float[] perDiscUnComp;
    private float[] perDiscComp;
    
    private int discCount;
    private int discCountUnComp;
    private int discCountComp;
    private int lowCount;
    private int lowUnCompCount;
    private int lowCompCount;
    private int lowRegions;
    private int lowUnCompRegions;
    private int lowCompRegions;
    
    private String[] readNames; //Holds read names        
    private String[] readSeq;    //Holds read sequences
    private int[] readStarts;  //Holds read starts
    private boolean[] readDirection;    //Holds read directions
    
    private String inName;    //Holds input filename
    private String outName;   //Holds output filename
    private String contigName;//Holds contig name
    
    @Override
    public Void doInBackground() throws FileNotFoundException, IOException{
        
        this.inputFile = new File(this.inName);
        
        Scanner contigScanner = new Scanner(new FileReader(this.inputFile));
        
        mw.setProcessing("Getting Contig Information");
        //Find the contig start
        while(contigScanner.hasNextLine()){
            
            String find = contigScanner.findInLine("CO " + this.contigName);
            
            if(find != null){
                
                this.assemblySize = contigScanner.nextInt();
                
                this.assembly = new char[this.assemblySize];
                this.totalDepth = new int[this.assemblySize];
                this.discDepth = new int[this.assemblySize];
                this.unCompDepth = new int[this.assemblySize];
                this.unCompDiscDepth = new int[this.assemblySize];
                this.compDepth = new int[this.assemblySize];
                this.compDiscDepth = new int[this.assemblySize];
                this.perDisc = new float[this.assemblySize];
                this.perDiscUnComp = new float[this.assemblySize];
                this.perDiscComp = new float[this.assemblySize];
                
                this.numReads = contigScanner.nextInt();
                this.readNames = new String[this.numReads];
                this.readSeq = new String[this.numReads];
                Arrays.fill(readSeq, "");
                this.readStarts = new int[this.numReads];
                this.readDirection = new boolean[this.numReads];
                Arrays.fill(this.readDirection, false);
                
                break;
            }
            contigScanner.nextLine();
        }
        
        //Write the Consensus
        mw.updateProgressText("Getting Contig Consensus");
        
        int pos = 0;
        contigScanner.nextLine();
        String current = contigScanner.nextLine();
        current = current.toUpperCase();
        
        while (!"".equals(current)){
            for (int i = 0; i < current.length();i++){
                this.assembly[i+pos] = current.charAt(i);
            }
            pos += current.length();
            current = contigScanner.nextLine();
            current = current.toUpperCase();
        }
        
        mw.updateProgressText("Discovering Readnames");
        contigScanner = Utilities.findPhrase(contigScanner, "AF");
        
        for (int i = 0; i < this.numReads; i++){
            
            readNames[i] = contigScanner.next();
            
            if("U".equals(contigScanner.next())){
                this.readDirection[i] =true;
            }        
            
            this.readStarts[i] = Integer.parseInt(contigScanner.next());
            
            //Skips BS lines
            String BS = "BS";
            String nextAF = contigScanner.next();
            while (nextAF.equals(BS)){
                contigScanner.next();
                contigScanner.next();
                contigScanner.next();
                nextAF = contigScanner.next();
            }
            
        }
        mw.setupProgress(this.numReads);
        for (int i = 0; i < this.numReads; i++){
            
            if(this.readNames[i].equals(this.contigName)){
                continue;
            }
            
            if (i > 0){
                contigScanner = Utilities.findPhrase(contigScanner, "RD " + readNames[i]);
                if (contigScanner == null){
                    break;
                }
            }
            
            contigScanner.nextLine();
            
            String read = "";
            
            current = contigScanner.nextLine();
            
            while (!"".equals(current)){
                read += current;
                current = contigScanner.nextLine();
            }
            
            read = read.toUpperCase();
            
            int readStart = readStarts[i];
            boolean readDir = readDirection[i];

            for(int j = 0; j < read.length();j++){
                
                if ((readStart < 1)&&(j == 0)){
                    j = Math.abs(readStart);
                }
                //if not at contig start yet, continue
                if (readStart + j < 1) {
                    continue;
                }
                //If outside bounds, break, we are done.
                if (readStart + j > assembly.length) {
                    break;
                }
                
                totalDepth[j+readStart-1] += 1;
                
                if (!(assembly[j+readStart-1]==read.charAt(j))){
                    discDepth[j+readStart-1] += 1;
                }
                
                if (readDir){
                    
                    unCompDepth[j + readStart - 1] += 1;
                    
                    if (!(assembly[j + readStart - 1] == read.charAt(j))){
                        unCompDiscDepth[j + readStart - 1] += 1;
                    }
                    
                }else if (!readDir){
                    
                    compDepth[j + readStart - 1] += 1;
                    
                    if (!(assembly[j + readStart - 1] == read.charAt(j))){
                        compDiscDepth[j + readStart - 1] += 1;
                    }
                }
                //System.out.println();
            }
            //System.out.println("End Read");
            publish(1);
            
        }
        
        for (int i =0; i < totalDepth.length; i++){
            if (totalDepth[i] == 0 || unCompDepth[i] == 0 || compDepth[i] == 0){
                //System.out.println(i + ": " + totalDepth[i]);
                //System.out.println(i + ": " + unCompDepth[i]);
                //System.out.println(i + ": " + compDepth[i]);
            }
        }
        this.Analysis();
        mw.setIdle();
        return null;
    }
    
    @Override
    public void process(List<Integer> chunks){
        for (Integer i : chunks) {
            curRead += i;
            this.mw.updateProgress(curRead);
            if (curRead == this.numReads)
                mw.updateProgressText("Performing Analyses");
        }
    }
    
    AssemblyWorker(String paramInputFileName, String paramOutputFileName, String paramContig, AceUtilSettings paramAceUtilSettings, MainWindow paramMainWindow) throws FileNotFoundException{
        
        this.settings = paramAceUtilSettings;
        
        this.mw = paramMainWindow;
        
        this.inName = paramInputFileName;
        this.outName = paramOutputFileName;
        this.contigName = paramContig;
        
    }
    
    public void Analysis() throws IOException{
        
        for(int i=0;i < perDisc.length;i++){
            perDisc[i] = (float)discDepth[i]/totalDepth[i];
        }
        
        for(int i=0;i < perDiscUnComp.length;i++){
            perDiscUnComp[i] = (float)unCompDiscDepth[i]/unCompDepth[i];
        }
        
        for(int i=0;i < perDiscComp.length;i++){
            perDiscComp[i] = (float)compDiscDepth[i]/compDepth[i];
        }

        if (!settings.getWriteOut()){
            
            if (settings.getDiscFind()){
                discCount = 0;
                for(int i = 0;i < perDisc.length;i++){
                    if ((perDisc[i] * 100) >= (settings.getDiscCutoff())){
                        discCount++;
                    }
                }
                System.out.println("discFind analysis finished - " + discCount + " discrepancies at " + settings.getDiscCutoff() + "% cutoff.");
            }
            
            if (settings.getDiscStrand()){
                
                discCountUnComp = 0;
                for(int i = 0;i < perDiscUnComp.length;i++){
                    if ((perDiscUnComp[i] * 100) >= (settings.getDiscStrandCutoff())){
                        discCountUnComp++;
                    }
                }
                System.out.println("discStrand analysis finished - " + discCountUnComp + " FORWARD discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");
                
                discCountComp = 0;
                for(int i = 0;i < perDiscComp.length;i++){
                    if ((perDiscComp[i] * 100) >= (settings.getDiscStrandCutoff())){
                        discCountComp++;
                    }
                }
                System.out.println("discStrand analysis finished - " + discCountComp + " REVERSE discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");
            }
            
            if (settings.getLowCov()){
                lowCount = 0;
                lowRegions = 0;
                boolean prevLow = false;
                for(int i = 0;i < totalDepth.length;i++){
                    if (prevLow){
                        if (totalDepth[i] <= settings.getLowCutoff()){
                            lowCount++;
                            continue;
                        }
                        lowRegions++;
                        prevLow = false;

                    }
                    if (totalDepth[i] <= settings.getLowCutoff()){
                        lowCount++;
                        prevLow = true;
                    }
                }
                if (prevLow){
                    lowRegions++;
                }
                System.out.println("lowCov analysis finished - " + lowCount + " low coverage bases and " + lowRegions + " regions of low coverage at " + settings.getLowCutoff() + "x cutoff.");
            }
            
            if (settings.getLowStrand()){
                System.out.println("lowStrand no writeOut");
                
                //FORWARD
                lowUnCompCount = 0;
                lowUnCompRegions = 0;
                boolean prevLow = false;
                for(int i = 0;i < unCompDepth.length;i++){
                    if (prevLow){
                        if (unCompDepth[i] <= settings.getLowStrandCutoff()){
                            lowUnCompCount++;
                            continue;
                        }
                        lowUnCompRegions++;
                        prevLow = false;

                    }
                    if (unCompDepth[i] <= settings.getLowStrandCutoff()){
                        lowUnCompCount++;
                        prevLow = true;
                    }
                }
                if (prevLow){
                    lowUnCompRegions++;
                }
                System.out.println("lowStrand analysis finished - " + lowUnCompCount + " FORWARD low coverage bases and " + lowUnCompRegions + " regions of low coverage at " + settings.getLowStrandCutoff() + "x cutoff.");
                
                //REVERSE
                lowCompCount = 0;
                lowCompRegions = 0;
                prevLow = false;
                for(int i = 0;i < compDepth.length;i++){
                    if (prevLow){
                        if (compDepth[i] <= settings.getLowStrandCutoff()){
                            lowCompCount++;
                            continue;
                        }
                        lowCompRegions++;
                        prevLow = false;

                    }
                    if (compDepth[i] <= settings.getLowStrandCutoff()){
                        lowCompCount++;
                        prevLow = true;
                    }
                }
                if (prevLow){
                    lowCompRegions++;
                }
                System.out.println("lowStrand analysis finished - " + lowCompCount + " REVERSE low coverage bases and " + lowCompRegions + " regions of low coverage at " + settings.getLowStrandCutoff() + "x cutoff.");
            }
        }
        
        if (settings.getWriteOut()){

                System.out.println("Making the new file");

                File writeOut = new File(this.outName);

                Utilities.copyFile(inputFile, writeOut);

                FileWriter getFile = new FileWriter(writeOut, true);
                BufferedWriter out = new BufferedWriter(getFile);

                if (settings.getDiscFind()){
                    discCount = 0;
                    for(int i = 0;i < perDisc.length;i++){
                        if ((perDisc[i] * 100) >= (settings.getDiscCutoff())){
                            discCount++;
                            out.newLine();
                            out.write("CT{");
                            out.newLine();
                            out.write(this.contigName + " comment discFind " + (i + 1) + " "+ (i + 1) + " 130912:160000");
                            out.newLine();
                            out.write("COMMENT{");
                            out.newLine();
                            out.write((perDisc[i] * 100) + "% discrepancies at this position greater than cutoff " + settings.getDiscCutoff() + "%");
                            out.newLine();
                            out.write("C}");
                            out.newLine();
                            out.write("}");
                            out.newLine();
                        }
                    }
                    System.out.println("discFind analysis finished - " + discCount + " discrepancies at " + settings.getDiscCutoff() + "% cutoff.");
                }
            
                if (settings.getDiscStrand()){

                    System.out.println("discStrand");
                    discCountUnComp = 0;
                    for(int i = 0;i < perDiscUnComp.length;i++){
                        if ((perDiscUnComp[i] * 100) >= (settings.getDiscStrandCutoff())){
                            discCountUnComp++;
                            out.newLine();
                            out.write("CT{");
                            out.newLine();
                            out.write(this.contigName + " comment discStrand " + (i + 1) + " "+ (i + 1) + " 130912:160000");
                            out.newLine();
                            out.write("COMMENT{");
                            out.newLine();
                            out.write((perDiscUnComp[i] * 100) + "% FORWARD discrepancies at this position greater than cutoff " + settings.getDiscStrandCutoff() + "%");
                            out.newLine();
                            out.write("C}");
                            out.newLine();
                            out.write("}");
                            out.newLine();
                        }
                    }
                    System.out.println("discFind analysis finished - " + discCountUnComp + " FORWARD discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");

                    discCountComp = 0;
                    for(int i = 0;i < perDiscComp.length;i++){
                        if ((perDiscComp[i] * 100) >= (settings.getDiscStrandCutoff())){
                            discCountComp++;
                            out.newLine();
                            out.write("CT{");
                            out.newLine();
                            out.write(this.contigName + " comment AceUtil " + (i + 1) + " "+ (i + 1) + " 130912:160000");
                            out.newLine();
                            out.write("COMMENT{");
                            out.newLine();
                            out.write((perDiscComp[i] * 100) + "% REVERSE discrepancies at this position greater than cutoff " + settings.getDiscStrandCutoff() + "%");
                            out.newLine();
                            out.write("C}");
                            out.newLine();
                            out.write("}");
                            out.newLine();
                        }
                    }
                    System.out.println("discFind analysis finished - " + discCountComp + " REVERSE discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");

                }

                if (settings.getLowCov()){
                    lowCount = 0;
                    lowRegions = 0;
                    int regStart = 0;
                    int regCov = 0;
                    int regSize = 0;
                    boolean prevLow = false;
                    
                    for(int i = 0;i < totalDepth.length;i++){
                        if (prevLow){
                            if (totalDepth[i] <= settings.getLowCutoff()){
                                regCov += totalDepth[i];
                                regSize++;
                                lowCount++;
                                continue;
                            }
                            
                            lowRegions++;
                            
                            out.newLine();
                            out.write("CT{");
                            out.newLine();
                            out.write(this.contigName + " comment AceUtil " + (regStart + 1) + " "+ (i) + " 130912:160000");
                            out.newLine();
                            out.write("COMMENT{");
                            out.newLine();
                            out.write("Avg " + (regCov/regSize) + "x depth coverage at position " + (regStart + 1) + "-" + i + " lower than cutoff " + settings.getLowCutoff() + "x");
                            out.newLine();
                            out.write("C}");
                            out.newLine();
                            out.write("}");
                            out.newLine();
                            
                            prevLow = false;
                            regCov = 0;
                            regSize = 0;
                        }
                        if (totalDepth[i] <= settings.getLowCutoff()){
                            lowCount++;
                            regCov += totalDepth[i];
                            regSize++;
                            prevLow = true;
                            regStart = i;
                        }
                    }
                    //Catches right end low coverage
                    if (prevLow){

                            lowRegions++;

                            out.newLine();
                            out.write("CT{");
                            out.newLine();
                            out.write(this.contigName + " comment AceUtil " + (regStart + 1) + " "+ (totalDepth.length) + " 130912:160000");
                            out.newLine();
                            out.write("COMMENT{");
                            out.newLine();
                            out.write("Avg " + (regCov/regSize) + "x depth coverage at position " + (regStart + 1) + "-" + (totalDepth.length) + " lower than cutoff " + settings.getLowCutoff() + "x");
                            out.newLine();
                            out.write("C}");
                            out.newLine();
                            out.write("}");
                            out.newLine();

                    }

                    System.out.println("lowCov analysis finished - " + lowCount + " low coverage bases and " + lowRegions + " regions of low coverage at " + settings.getLowCutoff() + "x cutoff.");
                }
                
                if (settings.getLowStrand()){
                    //FORWARD
                    lowUnCompCount = 0;
                    lowUnCompRegions = 0;
                    int regStart = 0;
                    int regCov = 0;
                    int regSize = 0;
                    boolean prevLow = false;
                    
                    for(int i = 0;i < unCompDepth.length;i++){
                        if (prevLow){
                            if (unCompDepth[i] <= settings.getLowStrandCutoff()){
                                regCov += unCompDepth[i];
                                regSize++;
                                lowUnCompCount++;
                                continue;
                            }
                            
                            lowUnCompRegions++;
                            
                            out.newLine();
                            out.write("CT{");
                            out.newLine();
                            out.write(this.contigName + " comment AceUtil " + (regStart + 1) + " "+ (i) + " 130912:160000");
                            out.newLine();
                            out.write("COMMENT{");
                            out.newLine();
                            out.write("Avg " + (regCov/regSize) + "x FORWARD depth coverage at position " + (regStart + 1) + "-" + i + " lower than cutoff " + settings.getLowStrandCutoff() + "x");
                            out.newLine();
                            out.write("C}");
                            out.newLine();
                            out.write("}");
                            out.newLine();
                            
                            prevLow = false;
                            regCov = 0;
                            regSize = 0;

                        }
                        if (unCompDepth[i] <= settings.getLowStrandCutoff()){
                            lowUnCompCount++;
                            regCov += unCompDepth[i];
                            regSize++;
                            prevLow = true;
                            regStart = i;
                        }
                    }
                    if (prevLow){
                        
                        lowUnCompRegions++;
                        
                        out.newLine();
                        out.write("CT{");
                        out.newLine();
                        out.write(this.contigName + " comment AceUtil " + (regStart + 1) + " "+ (unCompDepth.length) + " 130912:160000");
                        out.newLine();
                        out.write("COMMENT{");
                        out.newLine();
                        out.write("Avg " + (regCov/regSize) + "x FORWARD depth coverage at position " + (regStart + 1) + "-" + (unCompDepth.length) + " lower than cutoff " + settings.getLowStrandCutoff() + "x");
                        out.newLine();
                        out.write("C}");
                        out.newLine();
                        out.write("}");
                        out.newLine();
                    }
                    System.out.println("lowStrand analysis finished - " + lowUnCompCount + " FORWARD low coverage bases and " + lowUnCompRegions + " regions of low coverage at " + settings.getLowStrandCutoff() + "x cutoff.");

                    //REVERSE
                    lowCompCount = 0;
                    lowCompRegions = 0;
                    regStart = 0;
                    regCov = 0;
                    regSize = 0;
                    prevLow = false;
                    
                    for(int i = 0;i < compDepth.length;i++){
                        if (prevLow){
                            if (compDepth[i] <= settings.getLowStrandCutoff()){
                                regCov += compDepth[i];
                                regSize++;
                                lowCompCount++;
                                continue;
                            }
                            
                            lowCompRegions++;
                            
                            out.newLine();
                            out.write("CT{");
                            out.newLine();
                            out.write(this.contigName + " comment AceUtil " + (regStart + 1) + " "+ (i) + " 130912:160000");
                            out.newLine();
                            out.write("COMMENT{");
                            out.newLine();
                            out.write("Avg " + (regCov/regSize) + "x REVERSE depth coverage at position " + (regStart + 1) + "-" + i + " lower than cutoff " + settings.getLowStrandCutoff() + "x");
                            out.newLine();
                            out.write("C}");
                            out.newLine();
                            out.write("}");
                            out.newLine();
                            
                            prevLow = false;
                            regCov = 0;
                            regSize = 0;
                            

                        }
                        if (compDepth[i] <= settings.getLowStrandCutoff()){
                            lowCompCount++;
                            regCov += compDepth[i];
                            regSize++;
                            prevLow = true;
                            regStart = i;
                        }
                    }
                    if (prevLow){
                        lowCompRegions++;
                        
                        out.newLine();
                        out.write("CT{");
                        out.newLine();
                        out.write(this.contigName + " comment AceUtil " + (regStart + 1) + " "+ (compDepth.length) + " 130912:160000");
                        out.newLine();
                        out.write("COMMENT{");
                        out.newLine();
                        out.write("Avg " + (regCov/regSize) + "x REVERSE depth coverage at position " + (regStart + 1) + "-" + (compDepth.length) + " lower than cutoff " + settings.getLowStrandCutoff() + "x");
                        out.newLine();
                        out.write("C}");
                        out.newLine();
                        out.write("}");
                        out.newLine();
                    }
                    System.out.println("lowStrand analysis finished - " + lowCompCount + " REVERSE low coverage bases and " + lowCompRegions + " regions of low coverage at " + settings.getLowStrandCutoff() + "x cutoff.");
                }

                out.close();
            }
        }
}
    