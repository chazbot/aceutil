// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// Contig class for Holding Contig Data.

package AceUtil;

public class Contig {
    
    public String name;
    public int numBases;
    public int numReads;
    public int numSegs;
    public boolean unComp;
    
    public Contig(String paramName, int paramBases, int paramReads, int paramSegs, boolean paramUnComp){
        this.name = paramName;
        this.numBases = paramBases;
        this.numReads = paramReads;
        this.numSegs = paramSegs;
        this.unComp = paramUnComp;
    }
    
    @Override
    public String toString(){
        return this.name + " #R:" + String.valueOf(this.numReads);
    }
    
}
