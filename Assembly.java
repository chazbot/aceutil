// Copyright (c) 2012, 2013 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// Assembly class for building and analyzing version 2.0

package AceUtil;

//Import Block
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Assembly {
    
    AceUtilSettings settings;
    
    private int assemblySize;           //length of assembly (with pads)
    private int numReads;
    
    private File inputFile;
    
    private char[] assembly;    //Holds assembly consensus
    private int[] totalDepth;
    private int[] discDepth;
    private int[] unCompDepth;
    private int[] unCompDiscDepth;
    private int[] compDepth;
    private int[] compDiscDepth;
    private float[] perDisc;
    private float[] perDiscUnComp;
    private float[] perDiscComp;
    
    private int discCount;
    private int discCountUnComp;
    private int discCountComp;
    private int lowCount;
    private int lowUnCompCount;
    private int lowCompCount;
    private int lowRegions;
    private int lowUnCompRegions;
    private int lowCompRegions;
    
    private String[] readNames; //Holds read names        
    private String[] readSeq;    //Holds read sequences
    private int[] readStarts;  //Holds read starts
    private boolean[] readDirection;    //Holds read directions
    
    private String inName;    //Holds input filename
    private String outName;
    private String contigName;
    
    Assembly(String paramInputFileName, String paramOutputFileName, String paramContig, AceUtilSettings paramAceUtilSettings) throws FileNotFoundException{
        
        this.settings = paramAceUtilSettings;
        
        this.inName = paramInputFileName;
        this.outName = paramOutputFileName;
        this.contigName = paramContig;
        
        this.inputFile = new File(this.inName);
        
        Scanner contigScanner = new Scanner(new FileReader(inputFile));
        
        //Find the contig start
        while(contigScanner.hasNextLine()){
            
            String find = contigScanner.findInLine("CO " + paramContig);
            
            if(find != null){
                
                this.assemblySize = contigScanner.nextInt();
                
                this.assembly = new char[this.assemblySize];
                this.totalDepth = new int[this.assemblySize];
                this.discDepth = new int[this.assemblySize];
                this.unCompDepth = new int[this.assemblySize];
                this.unCompDiscDepth = new int[this.assemblySize];
                this.compDepth = new int[this.assemblySize];
                this.compDiscDepth = new int[this.assemblySize];
                this.perDisc = new float[this.assemblySize];
                this.perDiscUnComp = new float[this.assemblySize];
                this.perDiscComp = new float[this.assemblySize];
                
                this.numReads = contigScanner.nextInt();
                this.readNames = new String[this.numReads];
                this.readSeq = new String[this.numReads];
                Arrays.fill(readSeq, "");
                this.readStarts = new int[this.numReads];
                this.readDirection = new boolean[this.numReads];
                Arrays.fill(this.readDirection, false);
                
                break;
            }
            contigScanner.nextLine();
        }
        
        //Write the Consensus
        
        int pos = 0;
        contigScanner.nextLine();
        String current = contigScanner.nextLine();
        current = current.toUpperCase();
        
        while (!"".equals(current)){
            for (int i = 0; i < current.length();i++){
                this.assembly[i+pos] = current.charAt(i);
            }
            pos += current.length();
            current = contigScanner.nextLine();
            current = current.toUpperCase();
        }
        
        contigScanner = Utilities.findPhrase(contigScanner, "AF");
        
        for (int i = 0; i < this.numReads; i++){
            
            readNames[i] = contigScanner.next();
            
            if("U".equals(contigScanner.next())){
                this.readDirection[i] =true;
            }        
            
            this.readStarts[i] = Integer.parseInt(contigScanner.next());
            
            //Skips BS lines
            String BS = "BS";
            String nextAF = contigScanner.next();
            while (nextAF.equals(BS)){
                contigScanner.next();
                contigScanner.next();
                contigScanner.next();
                nextAF = contigScanner.next();
            }
            
        }
        
        this.buildReads(contigScanner);
        
        
    }
    
    public void buildReads(Scanner paramScan) throws FileNotFoundException{
            
        for (int i = 0; i < this.numReads; i++){
            
            if (i > 0){
                paramScan = Utilities.findPhrase(paramScan, "RD " + readNames[i]);
                if (paramScan == null){
                    break;
                }
            }
            
            paramScan.nextLine();
            
            String current;
            
            current = paramScan.nextLine();
            
            while (!"".equals(current)){
                this.readSeq[i] += current;
                current = paramScan.nextLine();
            }
            
            this.readSeq[i] = this.readSeq[i].toUpperCase();
            
        }
            
    }
    
    public void Analysis() throws IOException{
        
        for(int j = 0; j < readNames.length;j++){

            String read = readSeq[j];
            int readStart = readStarts[j];
            boolean readDir = readDirection[j];

            for(int i = 0; i < read.length();i++){

                if ((readStart < 1)&&(i == 0)){
                    i = Math.abs(readStart);
                }
                if (readStart + i < 1) {
                    continue;
                }
                if (readStart + i > assembly.length) {
                    System.out.println("BREAK");
                    break;
                }
                totalDepth[i+readStart-1] += 1;
                if (!(assembly[i+readStart-1]==read.charAt(i))){
                    discDepth[i+readStart-1] += 1;
                }
                
                if (readDir){
                    unCompDepth[i + readStart - 1] += 1;
                    if (!(assembly[i+readStart-1]==read.charAt(i))){
                        unCompDiscDepth[i + readStart - 1] += 1;
                    }
                }else if (!readDir){
                    compDepth[i + readStart - 1] += 1;
                    if (!(assembly[i+readStart-1]==read.charAt(i))){
                        compDiscDepth[i + readStart - 1] += 1;
                    }
                }
                
            }
        }

        //Do discrepant counts
        for(int i=0;i < perDisc.length;i++){
            perDisc[i] = (float)discDepth[i]/totalDepth[i];
        }
        
        for(int i=0;i < perDiscUnComp.length;i++){
            perDiscUnComp[i] = (float)unCompDiscDepth[i]/unCompDepth[i];
        }
        
        for(int i=0;i < perDiscComp.length;i++){
            perDiscComp[i] = (float)compDiscDepth[i]/compDepth[i];
        }
        
        //Here begins the analysis and writeouts
        String writeString;
        BufferedWriter out;
        
        if (settings.getWriteOut()){
            File writeOut = new File(this.outName);

            Utilities.copyFile(inputFile, writeOut);

            FileWriter getFile = new FileWriter(writeOut, true);
            out = new BufferedWriter(getFile);
            
        }else{
            out = null;
        }
        
        //Do discFind
        if (settings.getDiscFind()){
            discCount = 0;
            for(int i = 0;i < perDisc.length;i++){
                if ((perDisc[i] * 100) >= (settings.getDiscCutoff())){
                    discCount++;
                    if(settings.getWriteOut()){
                        writeString = (perDisc[i] * 100) + "% discrepancies at this position greater than cutoff " + settings.getDiscCutoff() + "%";
                        Utilities.writeComment(out, i, writeString, this.contigName);
                    }
                }
            }
            System.out.println("discFind analysis finished - " + discCount + " discrepancies at " + settings.getDiscCutoff() + "% cutoff.");
        }
        
        //Do discStrand
        if (settings.getDiscStrand()){
            discCountUnComp = 0;
            for(int i = 0;i < perDiscUnComp.length;i++){
                if ((perDiscUnComp[i] * 100) >= (settings.getDiscStrandCutoff())){
                    discCountUnComp++;
                    if(settings.getWriteOut()){
                        writeString = (perDiscUnComp[i] * 100) + "% FORWARD discrepancies at this position greater than cutoff " + settings.getDiscStrandCutoff() + "%";
                        Utilities.writeComment(out, i, writeString, this.contigName);
                    }
                }
            }
            System.out.println("discFind analysis finished - " + discCountUnComp + " FORWARD discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");

            discCountComp = 0;
            for(int i = 0;i < perDiscComp.length;i++){
                if ((perDiscComp[i] * 100) >= (settings.getDiscStrandCutoff())){
                    discCountComp++;
                    if(settings.getWriteOut()){
                        writeString = (perDiscComp[i] * 100) + "% REVERSE discrepancies at this position greater than cutoff " + settings.getDiscStrandCutoff() + "%";
                        Utilities.writeComment(out, i, writeString, this.contigName);
                    }
                }
            }
        }
            System.out.println("discFind analysis finished - " + discCountComp + " REVERSE discrepancies at " + settings.getDiscStrandCutoff() + "% cutoff.");
            
        //Do lowCov
        if (settings.getLowCov()){
            lowCount = 0;
            lowRegions = 0;
            int regStart = 0;
            int regCov = 0;
            int regSize = 0;
            boolean prevLow = false;

            for(int i = 0;i < totalDepth.length;i++){
                if (prevLow){
                    if (totalDepth[i] <= settings.getLowCutoff()){
                        regCov += totalDepth[i];
                        regSize++;
                        lowCount++;
                        continue;
                    }

                    lowRegions++;

                    if(settings.getWriteOut()){
                        writeString = "Avg " + (regCov/regSize) + "x depth coverage at position " + (regStart + 1) + "-" + i + " lower than cutoff " + settings.getLowCutoff() + "x";
                        Utilities.writeComment(out, (regStart + 1), i, writeString, this.contigName);
                    }
                    
                    regCov = 0;
                    regSize = 0;
                    prevLow = false;

                }
                if (totalDepth[i] <= settings.getLowCutoff()){
                    lowCount++;
                    regCov += totalDepth[i];
                    regSize++;
                    prevLow = true;
                    regStart = i;
                }
            }

            if (prevLow){

                    lowRegions++;
                    
                    if(settings.getWriteOut()){
                        writeString = "Avg " + (regCov/regSize) + "x depth coverage at position " + (regStart + 1) + "-" + unCompDepth.length + " lower than cutoff " + settings.getLowCutoff() + "x";
                        Utilities.writeComment(out, (regStart + 1), totalDepth.length, writeString, this.contigName);
                    }
                }
            System.out.println("lowCov analysis finished - " + lowCount + " low coverage bases and " + lowRegions + " regions of low coverage at " + settings.getLowCutoff() + "x cutoff.");
        }
    
        if (settings.getLowStrand()){
            //FORWARD
            lowUnCompCount = 0;
            lowUnCompRegions = 0;
            int regStart = 0;
            int regCov = 0;
            int regSize = 0;
            boolean prevLow = false;
            for(int i = 0;i < unCompDepth.length;i++){
                if (prevLow){
                    if (unCompDepth[i] <= settings.getLowStrandCutoff()){
                        regCov += unCompDepth[i];
                        regSize++;
                        lowUnCompCount++;
                        continue;
                    }
                    lowUnCompRegions++;
                    if(settings.getWriteOut()){
                        writeString = "Avg " + (regCov/regSize) + "x FORWARD depth coverage at position " + (regStart + 1) + "-" + i + " lower than cutoff " + settings.getLowStrandCutoff() + "x";
                        Utilities.writeComment(out, (regStart + 1), i, writeString, this.contigName);
                    }
                    prevLow = false;
                    regCov = 0;
                    regSize = 0;

                }
                if (unCompDepth[i] <= settings.getLowStrandCutoff()){
                    lowUnCompCount++;
                    regCov += unCompDepth[i];
                    prevLow = true;
                    regStart = i;
                    regSize++;
                }
            }
            if (prevLow){
                lowUnCompRegions++;

                if(settings.getWriteOut()){
                    writeString = "Avg " + (regCov/regSize) + "x FORWARD depth coverage at position " + (regStart + 1) + "-" + (unCompDepth.length) + " lower than cutoff " + settings.getLowStrandCutoff() + "x";
                    Utilities.writeComment(out, (regStart + 1), unCompDepth.length, writeString, this.contigName);
                }
            }
            System.out.println("lowStrand analysis finished - " + lowUnCompCount + " FORWARD low coverage bases and " + lowUnCompRegions + " regions of low coverage at " + settings.getLowStrandCutoff() + "x cutoff.");

            //REVERSE
            lowCompCount = 0;
            lowCompRegions = 0;
            regStart = 0;
            regCov = 0;
            regSize = 0;
            prevLow = false;
            for(int i = 0;i < compDepth.length;i++){
                if (prevLow){
                    if (compDepth[i] <= settings.getLowStrandCutoff()){
                        regCov += compDepth[i];
                        regSize++;
                        lowCompCount++;
                        continue;
                    }
                    lowCompRegions++;
                    if(settings.getWriteOut()){
                        writeString = "Avg " + (regCov/regSize) + "x REVERSE depth coverage at position " + (regStart + 1) + "-" + i + " lower than cutoff " + settings.getLowStrandCutoff() + "x";
                        Utilities.writeComment(out, (regStart + 1), i, writeString, this.contigName);
                    }
                    prevLow = false;
                    regCov = 0;
                    regSize = 0;

                }
                if (compDepth[i] <= settings.getLowStrandCutoff()){
                    lowCompCount++;
                    regCov += compDepth[i];
                    prevLow = true;
                    regStart = i;
                    regSize++;
                }
            }
            if (prevLow){
                lowCompRegions++;
                if(settings.getWriteOut()){
                    writeString = "Avg " + (regCov/regSize) + "x REVERSE depth coverage at position " + (regStart + 1) + "-" + (compDepth.length) + " lower than cutoff " + settings.getLowStrandCutoff() + "x";
                    Utilities.writeComment(out, (regStart + 1), compDepth.length, writeString, this.contigName);
                }
            }
            System.out.println("lowStrand analysis finished - " + lowCompCount + " REVERSE low coverage bases and " + lowCompRegions + " regions of low coverage at " + settings.getLowStrandCutoff() + "x cutoff.");
        }
        if(settings.getWriteOut()){
            out.close();
        }
    }
}
    