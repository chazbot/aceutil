// Copyright (c) 2012, 2013, 2014 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 13, 2013
// AceUtil class for starting the application.

package AceUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

public class AceUtil {
    
    public static void main(String args[]) throws FileNotFoundException, IOException{
        
        //Normal GUI Execution
        if (args.length == 0){
            AceUtilSettings aceUtilSettings = new AceUtilSettings();
            MainWindow mainWindow = new MainWindow(aceUtilSettings);
        
        //For command line use
        //usage: java -jar AceUtil.jar <INPATH> <OUTPATH>
        }else{
            
            System.out.println("Settings and MainWindow");
            AceUtilSettings aceUtilSettings = new AceUtilSettings();
            MainWindow mainWindow = new MainWindow(aceUtilSettings, true);
            
            System.out.println("init params");
            String openPath = args[0];
            String savePath = args[1];
            Contig[] contigs = null;
            
            try {
                //Open and save paths
                
                contigs = null;

                //Open the input file
                System.out.println("Open File");
                File tempFile = new File(openPath);
                System.out.println("File Open");
                
                System.out.println("Find Contigs");
                Scanner tempScan = new Scanner(tempFile);
                int contigCounter = 0;
                do{
                    switch (tempScan.next()) {
                        case "AS":
                            contigs = new Contig[tempScan.nextInt()];
                            System.out.println("#Contigs: " + contigs.length);
                            break;
                        case "CO":
                            System.out.println("Found Contig");
                            String tempName = tempScan.next();
                            int tempBases = Integer.parseInt(tempScan.next());
                            int tempReads = Integer.parseInt(tempScan.next());
                            int tempSegs = Integer.parseInt(tempScan.next());
                            boolean tempUnComp;
                            if(tempScan.next().equals("U")){
                                 tempUnComp = true;
                            }else{
                                tempUnComp = false;
                            }

                            contigs[contigCounter] = new Contig(tempName, tempBases, tempReads, tempSegs, tempUnComp);

                            contigCounter++;
                            break;
                    }
                    tempScan.nextLine();
                }while(tempScan.hasNextLine() && tempScan.hasNext() && (contigCounter < contigs.length));
            }catch (Exception e){
                JOptionPane.showMessageDialog(null, "Not a valid ACE file, please try another file.\n" + openPath, "Error!", JOptionPane.WARNING_MESSAGE);
            }
            
            if (contigs == null){
                JOptionPane.showMessageDialog(null, "No contigs found in ACE file, please try another file.\n" + openPath, "Error!", JOptionPane.WARNING_MESSAGE);
            }else{
                //Find the largest contig by reads
                int maxNR = 0;
                int maxNRc = 0;
                for(int i = 0; i < contigs.length;i++){
                    if (contigs[i].numReads > maxNR){
                        maxNR = contigs[i].numReads;
                        maxNRc = i;
                    }else{
                        continue;
                    }
                }
                System.out.println("Using Contig: " + contigs[maxNRc].name);
                AssemblyWorker aw = new AssemblyWorker(openPath, savePath, contigs[maxNRc].name, aceUtilSettings, mainWindow);
                System.out.println("Worker Defined");
                aw.doInBackground();
                System.out.println("Worker Executed");
                System.exit(0);
            }
        }
    }
    
}
