// Copyright (c) 2012, 2013, 2014 All Right Reserved, Hatfull Lab, University of Pittsburgh
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.  USE AT YOUR OWN RISK.
//

// Charles Bowman
// cab106@pitt.edu
// September 20, 2013
// MainWindow class for interfacing with the user.

package AceUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import javax.swing.*;
import javax.swing.JOptionPane;

public class MainWindow extends JFrame implements ActionListener, ItemListener{
    
    //Variables Declaration
    Contig[] contigs;
    Contig contig;
    String openPath, savePath;
    boolean setIn = false, setOut = false, analActive = false, contigFound, doneSearch;
    AceUtilSettings settings;
    
    //GUI Components
    JMenuBar mainMenuBar;                                   //Main Menu Bar
    JMenu jMenuFile;                                        //The File Menu
    JMenuItem jMenuItemSetInp;
    JMenuItem jMenuItemSetOut;
    JMenuItem jMenuItemAnalyze;
    JMenuItem jMenuItemQuit;
    JMenu jMenuPreferences;                                 //The Preferences Menu
    JMenu jMenuAnalysisVersion;                             //The Analysis version submenu
    ButtonGroup groupVersion;
    JRadioButtonMenuItem jRadioButtonMenuItemOnePointZero;
    JRadioButtonMenuItem jRadioButtonMenuItemTwoPointZero;
    JRadioButtonMenuItem jRadioButtonMenuItemThreePointZero;
    JMenu jMenuIncludedAnalyses;                            //The included analyses submenu
    JCheckBoxMenuItem jCheckboxMenuItemDiscFind;
    JCheckBoxMenuItem jCheckboxMenuItemDiscStrand;
    JCheckBoxMenuItem jCheckboxMenuItemLowCov;
    JCheckBoxMenuItem jCheckboxMenuItemLowStrand;
    JCheckBoxMenuItem jCheckboxMenuItemWrite;
    JMenuItem jMenuItemSettings;
    JMenu jMenuHelp;                                        //The Help menu
    JMenuItem jMenuItemAbout;
    JTextField jTextFieldInPath;                            //Input Path Textbox
    JButton jButtonSetIn;                                  //Input Path Button
    JTextField jTextFieldOutPath;                           //Output Path Textbox
    JButton jButtonSetOut;                                 //Output Path Button
    JComboBox jComboBoxContig;                              //Contig Selector
    JButton jButtonAnalyze;                                 //The Analyze Button
    JProgressBar jProgressBarProcessing;
    
    //Normal constructor for normal work
    public MainWindow(AceUtilSettings paramAceUtilSettings){
        
        this.settings = paramAceUtilSettings;
        
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
                
            }
        });
        this.setVisible(true);
    }
    
    //#GUIFREE
    public MainWindow(AceUtilSettings paramAceUtilSettings, boolean noGUI){
        
        this.settings = paramAceUtilSettings;
        
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
        this.setVisible(false);
        
    }
    
    //Builds the frame
    private void createAndShowGUI(){
        
        this.setTitle("AceUtil: ACE File Analysis");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        
        //Build the Menu bar System
        mainMenuBar = new JMenuBar();
      
        //The File Menu
        jMenuFile = new JMenu("File");
        jMenuItemSetInp = new JMenuItem("Set Input File");
        jMenuItemSetOut = new JMenuItem("Set Output File");
        jMenuItemAnalyze = new JMenuItem("Analyze");
        jMenuItemAnalyze.setEnabled(false);
        jMenuItemQuit = new JMenuItem("Quit");
        jMenuFile.add(jMenuItemSetInp);
        jMenuFile.add(jMenuItemSetOut);
        jMenuFile.addSeparator();
        jMenuFile.add(jMenuItemAnalyze);
        jMenuFile.addSeparator();
        jMenuFile.add(jMenuItemQuit);
        mainMenuBar.add(jMenuFile);
        
        //The Preferences Menu
        jMenuPreferences = new JMenu("Preferences");
            //The Analysis Version Submenu
            jMenuAnalysisVersion = new JMenu("Analysis Version");
            groupVersion = new ButtonGroup();
            jRadioButtonMenuItemOnePointZero = new JRadioButtonMenuItem("v1.0");
            jRadioButtonMenuItemTwoPointZero = new JRadioButtonMenuItem("v2.0");
            jRadioButtonMenuItemThreePointZero = new JRadioButtonMenuItem("v3.0, (Recommended)");
            groupVersion.add(jRadioButtonMenuItemOnePointZero);
            groupVersion.add(jRadioButtonMenuItemTwoPointZero);
            groupVersion.add(jRadioButtonMenuItemThreePointZero);
            jMenuAnalysisVersion.add(jRadioButtonMenuItemOnePointZero);
            jMenuAnalysisVersion.add(jRadioButtonMenuItemTwoPointZero);
            jMenuAnalysisVersion.add(jRadioButtonMenuItemThreePointZero);
            //The Included Analyses Submenu
            jMenuIncludedAnalyses = new JMenu("Included Analyses");
            jCheckboxMenuItemDiscFind = new JCheckBoxMenuItem("DiscFind");
            jCheckboxMenuItemDiscStrand = new JCheckBoxMenuItem("DiscStrand");
            jCheckboxMenuItemLowCov = new JCheckBoxMenuItem("LowCov");
            jCheckboxMenuItemLowStrand = new JCheckBoxMenuItem("LowStrand");
            jMenuIncludedAnalyses.add(jCheckboxMenuItemDiscFind);
            jMenuIncludedAnalyses.add(jCheckboxMenuItemDiscStrand);
            jMenuIncludedAnalyses.add(jCheckboxMenuItemLowCov);
            jMenuIncludedAnalyses.add(jCheckboxMenuItemLowStrand);
        //The other menu items
        jCheckboxMenuItemWrite = new JCheckBoxMenuItem("Write Output");
        jMenuItemSettings = new JMenuItem("Settings");
        jMenuPreferences.add(jMenuAnalysisVersion);
        jMenuPreferences.add(jMenuIncludedAnalyses);
        jMenuPreferences.addSeparator();
        jMenuPreferences.add(jCheckboxMenuItemWrite);
        jMenuPreferences.addSeparator();
        jMenuPreferences.add(jMenuItemSettings);
        mainMenuBar.add(jMenuPreferences);
        
        //The Help Menu
        jMenuHelp = new JMenu("Help");
        jMenuItemAbout = new JMenuItem("About");
        jMenuHelp.add(jMenuItemAbout);
        mainMenuBar.add(jMenuHelp);
        
        this.setJMenuBar(mainMenuBar);
        
        //Row 1, Input Settings
        Container inpContainer = new Container();
        inpContainer.setLayout(new FlowLayout(FlowLayout.RIGHT));
        inpContainer.add(new JLabel("Input File:  "));
        jTextFieldInPath = new JTextField("...");
        jTextFieldInPath.setEnabled(false);
        jButtonSetIn = new JButton("Set Input File");
        inpContainer.add(jTextFieldInPath);
        inpContainer.add(jButtonSetIn);
        this.add(inpContainer);
        
        //Row 2, Output Settings
        Container outContainer = new Container();
        outContainer.setLayout(new FlowLayout(FlowLayout.RIGHT));
        outContainer.add(new JLabel("Output File:  "));
        jTextFieldOutPath = new JTextField("...");
        jTextFieldOutPath.setEnabled(false);
        jButtonSetOut = new JButton("Set Output File");
        outContainer.add(jTextFieldOutPath);
        outContainer.add(jButtonSetOut);
        this.add(outContainer);
        
        //Row 3, Contig selection and GO button
        Container botContainer = new Container();
        botContainer.setLayout(new FlowLayout(FlowLayout.RIGHT));
        botContainer.add(new JLabel("Contig:  ")); 
        jComboBoxContig = new JComboBox();
        jComboBoxContig.setEnabled(false);
        jButtonAnalyze = new JButton("Analyze");
        jButtonAnalyze.setEnabled(false);
        botContainer.add(jComboBoxContig);
        botContainer.add(jButtonAnalyze);
        this.add(botContainer);
        
        //Row 4, Progress Bar
        Container barContainer = new Container();
        barContainer.setLayout(new FlowLayout(FlowLayout.RIGHT));
        jProgressBarProcessing = new JProgressBar();
        jProgressBarProcessing.setEnabled(false);
        jProgressBarProcessing.setString("Idle");
        jProgressBarProcessing.setStringPainted(true);
        jProgressBarProcessing.setPreferredSize(new Dimension(650, 25));
        barContainer.add(jProgressBarProcessing);
        this.add(barContainer);
        
        //Makes all 3 buttons the same size
        jButtonSetIn.setPreferredSize(jButtonSetOut.getPreferredSize());
        jButtonAnalyze.setPreferredSize(jButtonSetOut.getPreferredSize());
        jTextFieldInPath.setPreferredSize(new Dimension(400, 25));
        jTextFieldOutPath.setPreferredSize(new Dimension(400, 25));
        jComboBoxContig.setPreferredSize(new Dimension(400, 25));
        
        //Set the Buttons to their original setting state
        this.jCheckboxMenuItemDiscFind.setSelected(settings.getDiscFind());
        this.jCheckboxMenuItemDiscStrand.setSelected(settings.getDiscStrand());
        this.jCheckboxMenuItemLowCov.setSelected(settings.getLowCov());
        this.jCheckboxMenuItemLowStrand.setSelected(settings.getLowStrand());
        this.jCheckboxMenuItemWrite.setSelected(settings.getWriteOut());
        if(settings.getVersion() == 3){
            this.jRadioButtonMenuItemThreePointZero.setSelected(true);
            this.jCheckboxMenuItemLowStrand.setEnabled(true);
        }else if(settings.getVersion() == 2){
            this.jRadioButtonMenuItemTwoPointZero.setSelected(true);
            this.jCheckboxMenuItemLowStrand.setEnabled(true);
        }else{
            this.jRadioButtonMenuItemOnePointZero.setSelected(true);
            this.jCheckboxMenuItemLowStrand.setEnabled(false);
        }
        //Set up actionListeners
        jButtonSetIn.addActionListener(this);
        jMenuItemSetInp.addActionListener(this);
        jButtonSetOut.addActionListener(this);
        jMenuItemSetOut.addActionListener(this);
        jButtonAnalyze.addActionListener(this);
        jMenuItemAnalyze.addActionListener(this);
        jMenuItemQuit.addActionListener(this);
        jMenuItemSettings.addActionListener(this);
        jMenuItemAbout.addActionListener(this);
        
        //Set up itemListeners
        jCheckboxMenuItemWrite.addItemListener(this);
        jCheckboxMenuItemDiscFind.addItemListener(this);
        jCheckboxMenuItemDiscStrand.addItemListener(this);
        jCheckboxMenuItemLowCov.addItemListener(this);
        jCheckboxMenuItemLowStrand.addItemListener(this);
        jRadioButtonMenuItemOnePointZero.addItemListener(this);
        jRadioButtonMenuItemTwoPointZero.addItemListener(this);
        jRadioButtonMenuItemThreePointZero.addItemListener(this);
        
        //Housekeeping
        this.pack();
    }
    
    public void updateProgressText(String paramString){
        this.jProgressBarProcessing.setString(paramString);
        this.jProgressBarProcessing.setIndeterminate(true);
    }
    
    public void setupProgress(int paramInt){
        this.jProgressBarProcessing.setIndeterminate(false);
        this.jProgressBarProcessing.setMaximum(paramInt);
        this.jProgressBarProcessing.setString("Processing Read 1 of " + paramInt);
    }
    
    public void updateProgress(int paramInt){
        this.jProgressBarProcessing.setValue(paramInt);
        this.jProgressBarProcessing.setString("Processing Read " + paramInt + " of " + jProgressBarProcessing.getMaximum());
    }
    
    public void setProcessing(){
        this.mainMenuBar.setEnabled(false);
        this.jButtonSetIn.setEnabled(false);
        this.jButtonSetOut.setEnabled(false);
        this.jComboBoxContig.setEnabled(false);
        this.jProgressBarProcessing.setEnabled(false);
        this.jProgressBarProcessing.setIndeterminate(true);
        this.jProgressBarProcessing.setString("Processing, Please Wait");
        this.analActive = jButtonAnalyze.isEnabled();
        this.jButtonAnalyze.setEnabled(false);
    }
    
    public void setProcessing(String paramString){
        this.mainMenuBar.setEnabled(false);
        this.jButtonSetIn.setEnabled(false);
        this.jButtonSetOut.setEnabled(false);
        this.jComboBoxContig.setEnabled(false);
        this.jProgressBarProcessing.setEnabled(false);
        this.jProgressBarProcessing.setIndeterminate(true);
        this.jProgressBarProcessing.setString(paramString);
        this.analActive = jButtonAnalyze.isEnabled();
        this.jButtonAnalyze.setEnabled(false);
    }
    
    public void setIdle(){
        this.mainMenuBar.setEnabled(true);
        this.jButtonSetIn.setEnabled(true);
        this.jButtonSetOut.setEnabled(true);
        this.jButtonAnalyze.setEnabled(this.analActive);
        this.jComboBoxContig.setEnabled(true);
        this.jProgressBarProcessing.setIndeterminate(false);
        this.jProgressBarProcessing.setValue(0);
        this.jProgressBarProcessing.setEnabled(false);
        this.jProgressBarProcessing.setString("Idle");
    }
    
    public void setIdle(String paramString){
        this.mainMenuBar.setEnabled(true);
        this.jButtonSetIn.setEnabled(true);
        this.jButtonSetOut.setEnabled(true);
        this.jButtonAnalyze.setEnabled(this.analActive);
        this.jComboBoxContig.setEnabled(true);
        this.jProgressBarProcessing.setIndeterminate(false);
        this.jProgressBarProcessing.setValue(0);
        this.jProgressBarProcessing.setEnabled(false);
        this.jProgressBarProcessing.setString(paramString);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae){
        
        Object actionSource = ae.getSource();
        
        if (actionSource == jButtonSetIn || actionSource == jMenuItemSetInp){
            
            JFileChooser chooser = new JFileChooser();
            try{
                chooser.setCurrentDirectory(new java.io.File(settings.getPath()));
            }catch (Exception e){
                chooser.setCurrentDirectory(new java.io.File("~"));
            }
            chooser.setDialogTitle("Open ACE File");
            chooser.setMultiSelectionEnabled(false);
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setAcceptAllFileFilterUsed(true);

            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                
                setProcessing("Finding Contigs in ACE file.");
                
                openPath = (chooser.getSelectedFile()).getAbsolutePath();
                
                //Send a swingworker to get the contigs.
                SwingWorker worker = new SwingWorker<Contig[], Void>(){
                    @Override
                    public Contig[] doInBackground(){

                        try{
                            
                            Contig[] returnContig = null;

                            File tempFile = new File(openPath);

                            Scanner tempScan = new Scanner(tempFile);
                            int contigCounter = 0;
                            do{
                                switch (tempScan.next()) {
                                    case "AS":
                                        returnContig = new Contig[tempScan.nextInt()];
                                        break;
                                    case "CO":
                                        String tempName = tempScan.next();
                                        int tempBases = Integer.parseInt(tempScan.next());
                                        int tempReads = Integer.parseInt(tempScan.next());
                                        int tempSegs = Integer.parseInt(tempScan.next());
                                        boolean tempUnComp;
                                        if(tempScan.next().equals("U")){
                                             tempUnComp = true;
                                        }else{
                                            tempUnComp = false;
                                        }
                                        
                                        returnContig[contigCounter] = new Contig(tempName, tempBases, tempReads, tempSegs, tempUnComp);
                                        
                                        contigCounter++;
                                        contigFound = true;
                                        break;
                                }
                                tempScan.nextLine();
                            }while(tempScan.hasNextLine() && tempScan.hasNext() && (contigCounter < returnContig.length));
                            return returnContig;
                            
                        }catch (Exception e){
                            JOptionPane.showMessageDialog(null, "Not a valid ACE file, please try another file.", "Error!", JOptionPane.WARNING_MESSAGE);
                            return null;
                        }
                    }
                    @Override
                    public void done(){
                        try{
                            jComboBoxContig.removeAllItems();
                            contigs = get();
                            if (contigs == null){
                                //Do Nothing
                                JOptionPane.showMessageDialog(null, "No contigs found in ACE file, please try another file.", "Error!", JOptionPane.WARNING_MESSAGE);
                            }else{
                                
                                for (Contig contigName : contigs){
                                    jComboBoxContig.addItem(contigName);
                                }
                                jTextFieldInPath.setText(openPath);
                                jComboBoxContig.setEnabled(true);
                                setIn = true;
                                
                                try{
                                    int tempInt = Integer.parseInt(openPath.substring(openPath.lastIndexOf('.') + 1));
                                    tempInt++;
                                    savePath = openPath.substring(0, openPath.lastIndexOf('.') + 1) + tempInt;
                                    jTextFieldOutPath.setText(savePath);
                                    setOut = true;
                                    analActive = true;
                                    jMenuItemAnalyze.setEnabled(true);
                                    jButtonAnalyze.setEnabled(true);
                                }catch (Exception e){
                                    
                                }
                            }
                            setIdle();
                        }catch (InterruptedException ignore){}
                        catch (ExecutionException e){
                            JOptionPane.showMessageDialog(null, "Error processing ACE file, please try another file.", "Error!", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                };
                worker.execute();
                
            }
            
        }else if (actionSource == jButtonSetOut || actionSource == jMenuItemSetOut){
            
            JFileChooser chooser = new JFileChooser();

            chooser.setDialogType(JFileChooser.SAVE_DIALOG);
            chooser.setCurrentDirectory(new java.io.File("~"));
            chooser.setDialogTitle("Save output as?");
            chooser.setMultiSelectionEnabled(false);
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setAcceptAllFileFilterUsed(true);

            if (setIn) {
                chooser.setCurrentDirectory(new File(openPath));
            }
            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                savePath = ((chooser.getSelectedFile())).getAbsolutePath();
                jTextFieldOutPath.setText(savePath);
                setOut = true;
                if (setIn){
                    jMenuItemAnalyze.setEnabled(true);
                    jButtonAnalyze.setEnabled(true);
                }
            }
            
        }else if (actionSource == jButtonAnalyze || actionSource == jMenuItemAnalyze){
            if (this.settings.getVersion() == 3){
                try {
                    contig = (Contig)jComboBoxContig.getSelectedItem();
                    AssemblyWorker aw = new AssemblyWorker(this.openPath, this.savePath, this.contig.name, this.settings, this);
                    aw.execute();
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }else if (this.settings.getVersion() == 2){
                //Send a worker to do the analyses
                SwingWorker worker2 = new SwingWorker<Void, Void>(){
                    @Override
                    public Void doInBackground(){
                        File inputFile = new File(openPath);
                        contig = (Contig)jComboBoxContig.getSelectedItem();
                        try {
                            if(settings.getVersion() == 2){
                                Assembly currentAss = new Assembly(openPath, savePath, contig.name, settings);
       
                                currentAss.Analysis();
                            }else{
                                DiscFind.process(inputFile, contig.name, savePath, settings);
                            }
                        } catch (Exception e1){
                            JOptionPane.showMessageDialog(null, "Error Analyzing ACE file", "Error!", JOptionPane.WARNING_MESSAGE);
                            setIdle("Error In Calculations");
                        }
                        return null;
                    }
                    @Override
                    public void done(){
                        setIdle();
                    }
                };
                worker2.execute();
            }else{
                
            }
            
            
            
                
            
            
        }else if (actionSource == jMenuItemQuit){
            System.exit(0);
        }else if (actionSource == jMenuItemSettings){
            SettingsWindow SW = new SettingsWindow(settings);
        }else if (actionSource == jMenuItemAbout){
            JOptionPane.showMessageDialog(null, "AceUtil Ace File Utility\nB19-20140731\nWritten in the Hatfull Lab by Charles Bowman", "About", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    @Override
    public void itemStateChanged(ItemEvent ie){
        
        Object changeSource = ie.getSource();
        
        if (changeSource == jCheckboxMenuItemWrite){
            settings.setWriteOut(!settings.getWriteOut());
        }else if (changeSource == jCheckboxMenuItemDiscFind){
            settings.setDiscFind(!settings.getDiscFind());
        }else if (changeSource == jCheckboxMenuItemDiscStrand){
            settings.setDiscStrand(!settings.getDiscStrand());
        }else if (changeSource == jCheckboxMenuItemLowCov){
            settings.setLowCov(!settings.getLowCov());
        }else if (changeSource == jCheckboxMenuItemLowStrand){
            settings.setLowStrand(!settings.getLowStrand());
        }else if (changeSource == jRadioButtonMenuItemOnePointZero){
            settings.setVersion(1);
            jCheckboxMenuItemLowStrand.setEnabled(false);
        }else if (changeSource == jRadioButtonMenuItemTwoPointZero){
            settings.setVersion(2);
            jCheckboxMenuItemLowStrand.setEnabled(true);
        }else if (changeSource == jRadioButtonMenuItemThreePointZero){
            settings.setVersion(3);
            jCheckboxMenuItemLowStrand.setEnabled(true);
        }
        
        if ((settings.getDiscFind() || settings.getDiscStrand() || settings.getLowCov() || settings.getLowStrand()) && ((this.setIn && this.setOut) || (this.setIn && !settings.getWriteOut()))){
            jButtonAnalyze.setEnabled(true);
            jMenuItemAnalyze.setEnabled(true);
        }else{
            jButtonAnalyze.setEnabled(false);
            jMenuItemAnalyze.setEnabled(false);
        }
        
    }
    
}